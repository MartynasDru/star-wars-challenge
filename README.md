# Star Wars challenge
## Basic instructions
* Written with Typescript
* Uses Material-UI
* Styles with SCSS + CSS modules
* Unit tests Jest + Enzyme
* ESlint/Prettier setup + husky hooks to prevent user from commiting code with stylistic errors.

## Steps to run application on your machine
1) Clone application - `git clone https://gitlab.com/MartynasDru/star-wars-challenge.git`
2) Install node modules - `npm i`
3) Start local server - Application uses https://github.com/graphql/swapi-graphql as API and expects for it to run locally on `localhost:8080` so please make sure you have that running before other step.
4) Start application - `npm start`
5) You can now view star-wars-challenge in the browser on - `localhost:3000`