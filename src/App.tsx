import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import MovieCharacters from "pages/MovieCharacters/MovieCharacters";
import MovieCharacterFilms from "pages/MovieCharacterFilms/MovieCharacterFilms";
import MovieOpeningCrawl from "pages/MovieOpeningCrawl/MovieOpeningCrawl";

const App = () => (
  <BrowserRouter>
    <Switch>
      <Route path="/:characterId/:filmId">
        <MovieOpeningCrawl />
      </Route>
      <Route path="/:characterId">
        <MovieCharacterFilms />
      </Route>
      <Route path="/">
        <MovieCharacters />
      </Route>
    </Switch>
  </BrowserRouter>
);

export default App;
