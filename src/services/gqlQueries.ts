import { gql } from "@apollo/client";

export const GET_MOVIE_CHARACTERS = gql`
  {
    allPeople {
      people {
        name
        id
        filmConnection {
          totalCount
        }
        homeworld {
          name
        }
        species {
          name
        }
      }
    }
  }
`;

export const getCharacterMovies = (personId: string) =>
  gql`
    {
      person(id: "${personId}") {
        name
        filmConnection {
          films {
            id
            title
            director
            producers
            releaseDate
          }
        }
      }
    }
  `;

export const getFilmOpeningCrawl = (filmId: string) =>
  gql`
    {
      film(id: "${filmId}") {
        title,
        openingCrawl
        episodeID
      }
    }
  `;
