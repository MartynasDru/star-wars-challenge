import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useQuery } from "@apollo/client";

import { getFilmOpeningCrawl } from "services/gqlQueries";
import Loader from "components/Loader/Loader";
import { romanize } from "./utils/romanize";
import styles from "./MovieOpeningCrawl.module.scss";

const MovieOpeningCrawl = () => {
  const { filmId } = useParams<{ filmId: string }>();
  const [filmTitle, setFilmTitle] = useState("");
  const [openingCrawl, setOpeningCrawl] = useState("");
  const [romanEpisodeNumber, setRomanEpisodeNumber] = useState("");
  const { loading, data } = useQuery(getFilmOpeningCrawl(filmId));

  useEffect(() => {
    if (data) {
      setRomanEpisodeNumber(romanize(data.film.episodeID));
      setOpeningCrawl(data.film.openingCrawl);
      setFilmTitle(data.film.title);
    }
  }, [data]);

  return (
    <div className={styles["movie-opening-crawl"]}>
      {loading && <Loader isLight />}
      {!loading && data && (
        <div data-testid="movie-opening-crawl">
          <div className={styles["movie-opening-crawl__fade"]} />
          <div className={styles["movie-opening-crawl__opening-crawl"]}>
            <div className={styles["movie-opening-crawl__text-wrapper"]}>
              <p
                data-testid="film-episode"
                className={styles["movie-opening-crawl__episode"]}
              >
                Episode {romanEpisodeNumber}
              </p>
              <h1
                data-testid="film-title"
                className={styles["movie-opening-crawl__title"]}
              >
                {filmTitle}
              </h1>
              <p
                data-testid="crawl-text"
                className={styles["movie-opening-crawl__text"]}
              >
                {openingCrawl}
              </p>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default MovieOpeningCrawl;
