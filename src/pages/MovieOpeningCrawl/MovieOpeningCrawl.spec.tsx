import React from "react";
import Enzyme from "enzyme";

import MovieOpeningCrawl from "./MovieOpeningCrawl";

interface IQueryReturn {
  loading: boolean;
  data: null | object;
}

const useQueryReturnMock = {
  loading: false,
  data: null,
} as IQueryReturn;

jest.mock("@apollo/client", () => ({
  useQuery: () => useQueryReturnMock,
}));

jest.mock("react-router-dom", () => ({
  useParams: () => jest.fn(),
  useHistory: () => jest.fn(),
}));

jest.mock("services/gqlQueries", () => ({
  getFilmOpeningCrawl: () => jest.fn(),
}));

describe("MovieOpeningCrawl", () => {
  const setupTest = () => Enzyme.mount(<MovieOpeningCrawl />);

  beforeEach(() => {
    useQueryReturnMock.loading = false;
    useQueryReturnMock.data = null;
  });

  it("should not have loader and data by default", () => {
    const wrapper = setupTest();

    expect(wrapper.find("[data-testid='movie-opening-crawl']").length).toBe(0);
    expect(wrapper.find("[data-testid='loader']").length).toBe(0);
  });

  it("should show only loader if loading is true", () => {
    useQueryReturnMock.loading = true;
    const wrapper = setupTest();

    expect(wrapper.find("[data-testid='movie-opening-crawl']").length).toBe(0);
    expect(wrapper.find("[data-testid='loader']").length).toBe(1);
  });

  it("should show only opening crawl if loading is false and there is data loaded", () => {
    useQueryReturnMock.data = {
      film: {
        episodeID: "1",
        openingCrawl: "Opening crawl text",
        title: "Test title",
      },
    };
    const wrapper = setupTest();

    expect(wrapper.find("[data-testid='movie-opening-crawl']").length).toBe(1);
    expect(wrapper.find("[data-testid='loader']").length).toBe(0);
  });

  it("Opening crawl should have which episode is film in roman", () => {
    useQueryReturnMock.data = {
      film: {
        episodeID: "1",
        openingCrawl: "Opening crawl text",
        title: "Test title",
      },
    };
    const wrapper = setupTest();

    expect(wrapper.find("[data-testid='film-episode']").text()).toBe(
      "Episode I"
    );
  });

  it("Opening crawl should have film title", () => {
    const mockData = {
      film: {
        episodeID: "1",
        openingCrawl: "Opening crawl text",
        title: "Test title",
      },
    };
    useQueryReturnMock.data = mockData;
    const wrapper = setupTest();

    expect(wrapper.find("[data-testid='film-title']").text()).toBe(
      mockData.film.title
    );
  });

  it("Opening crawl should have opening crawl text", () => {
    const mockData = {
      film: {
        episodeID: "1",
        openingCrawl: "Opening crawl text",
        title: "Test title",
      },
    };
    useQueryReturnMock.data = mockData;
    const wrapper = setupTest();

    expect(wrapper.find("[data-testid='crawl-text']").text()).toBe(
      mockData.film.openingCrawl
    );
  });
});
