import { romanize } from "./romanize";

type ITestData = Array<Array<number | string>>;

const testData = [
  [1, "I"],
  [6, "VI"],
  [9, "IX"],
  [10, "X"],
  [11, "XI"],
  [16, "XVI"],
  [19, "XIX"],
  [21, "XXI"],
  [29, "XXIX"],
  [49, "XLIX"],
  [50, "L"],
  [51, "LI"],
  [99, "XCIX"],
  [100, "C"],
  [101, "CI"],
  [500, "D"],
  [999, "CMXCIX"],
  [1000, "M"],
  [1001, "MI"],
] as ITestData;

describe("romanize", () => {
  it("should convert correctly arabic number to roman", () => {
    testData.forEach((data) => {
      expect(romanize(Number(data[0]))).toBe(data[1]);
    });
  });
});
