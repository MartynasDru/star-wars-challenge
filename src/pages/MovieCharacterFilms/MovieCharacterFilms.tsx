import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import { useQuery } from "@apollo/client";

import { TABLE_COLUMNS } from "./utils/constants";
import PageContainer from "components/PageContainer/PageContainer";
import Table, { IAllRowsData } from "components/Table/Table";
import { getCharacterMovies } from "services/gqlQueries";
import Loader from "components/Loader/Loader";

interface ICharacterFilm {
  id: string;
  title: string;
  director: string;
  producers: Array<string>;
  releaseDate: string;
}

const MovieCharacterFilms = () => {
  const history = useHistory();
  const { characterId } = useParams<{ characterId: string }>();
  const [characterFilms, setCharacterFilms] = useState<IAllRowsData>([]);
  const [movieCharacter, setMovieCharacter] = useState<string>("");
  const { loading, data } = useQuery(getCharacterMovies(characterId));

  useEffect(() => {
    if (data) {
      setMovieCharacter(data.person.name);

      const updatedCharacterFilms = data.person.filmConnection.films.map(
        ({ title, releaseDate, director, producers, id }: ICharacterFilm) => ({
          rowId: id,
          rowData: [title, releaseDate, director, producers.join(", ")],
        })
      );
      setCharacterFilms(updatedCharacterFilms);
    }
  }, [data]);

  const handleRowClick = (filmId: string) => {
    history.push(`${characterId}/${filmId}`);
  };

  return (
    <PageContainer
      title={movieCharacter}
      description={`This page lists all movies that ${movieCharacter} was in.`}
    >
      {loading && <Loader />}
      {!loading && data && (
        <Table
          headerColumns={TABLE_COLUMNS}
          rowsData={characterFilms}
          onRowClick={handleRowClick}
        />
      )}
    </PageContainer>
  );
};

export default MovieCharacterFilms;
