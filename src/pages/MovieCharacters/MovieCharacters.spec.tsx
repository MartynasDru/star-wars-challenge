import React from "react";
import Enzyme from "enzyme";
import { Table } from "@material-ui/core";

import MovieCharacters from "./MovieCharacters";

interface IQueryReturn {
  loading: boolean;
  data: null | object;
}

const useQueryReturnMock = {
  loading: false,
  data: null,
} as IQueryReturn;

jest.mock("@apollo/client", () => ({
  useQuery: () => useQueryReturnMock,
}));

jest.mock("services/gqlQueries", () => ({
  GET_MOVIE_CHARACTERS: jest.fn(),
}));

describe("MovieCharacters", () => {
  const setupTest = () => Enzyme.mount(<MovieCharacters />);

  beforeEach(() => {
    useQueryReturnMock.loading = false;
    useQueryReturnMock.data = null;
  });

  it("should not have loader and data by default", () => {
    const wrapper = setupTest();

    expect(wrapper.find(Table).length).toBe(0);
    expect(wrapper.find("[data-testid='loader']").length).toBe(0);
  });

  it("should show only loader if loading is true", () => {
    useQueryReturnMock.loading = true;
    const wrapper = setupTest();

    expect(wrapper.find(Table).length).toBe(0);
    expect(wrapper.find("[data-testid='loader']").length).toBe(1);
  });

  it("should show only Table with data if loading is false and there is data loaded", () => {
    useQueryReturnMock.data = {
      allPeople: {
        people: [
          {
            id: "test-id",
            name: "test-name",
            filmConnection: {
              totalCount: 0,
            },
            homeworld: {
              name: "test-homeworld-name",
            },
          },
        ],
      },
    };
    const wrapper = setupTest();

    expect(wrapper.find(Table).length).toBe(1);
    expect(wrapper.find("[data-testid='loader']").length).toBe(0);
  });
});
