import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { useQuery } from "@apollo/client";

import PageContainer from "components/PageContainer/PageContainer";
import { TABLE_COLUMNS } from "./utils/constants";
import Table, { IAllRowsData } from "components/Table/Table";
import { GET_MOVIE_CHARACTERS } from "services/gqlQueries";
import Loader from "components/Loader/Loader";

interface IMovieCharacter {
  name: string;
  id: string;
  filmConnection: {
    totalCount: number;
  };
  homeworld: {
    name: string;
  };
  species: {
    name: string;
  };
}

const MovieCharacters = () => {
  const history = useHistory();
  const [allCharacters, setAllCharacters] = useState<IAllRowsData>([]);
  const { loading, data } = useQuery(GET_MOVIE_CHARACTERS);

  useEffect(() => {
    if (data) {
      const updatedAllCharacters = data.allPeople.people.map(
        (person: IMovieCharacter) => {
          return {
            rowId: person.id,
            rowData: [
              person.name,
              person.filmConnection.totalCount,
              person.homeworld.name,
              person.species?.name || "N/A",
            ],
          };
        }
      );
      setAllCharacters(updatedAllCharacters);
    }
  }, [data]);

  const handleRowClick = (characterId: string) => {
    history.push(`/${characterId}`);
  };

  return (
    <PageContainer
      title="Star Wars characters"
      description="This page displays a list of Star wars movie characters."
    >
      {loading && <Loader />}
      {!loading && data && (
        <Table
          headerColumns={TABLE_COLUMNS}
          rowsData={allCharacters}
          onRowClick={handleRowClick}
        />
      )}
    </PageContainer>
  );
};

export default MovieCharacters;
