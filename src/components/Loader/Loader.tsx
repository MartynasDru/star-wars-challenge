import React from "react";
import { CircularProgress } from "@material-ui/core";

import { classNames } from "utils/utils";
import styles from "./Loader.module.scss";

export interface ILoader {
  isLight?: boolean;
}

const Loader = ({ isLight }: ILoader) => (
  <div
    data-testid="loader"
    className={classNames([
      styles["loader"],
      isLight && styles["loader--light"],
    ])}
  >
    <div className={styles["loader__inner"]}>
      <CircularProgress classes={{ root: styles["loader__progress-circle"] }} />
      <p className={styles["loader__text"]}>Loading...</p>
    </div>
  </div>
);

export default Loader;
