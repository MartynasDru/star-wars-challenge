import React from "react";
import Enzyme from "enzyme";

import Loader, { ILoader } from "./Loader";

const defaultProps = {
  isLight: false,
} as ILoader;

describe("Loader", () => {
  const setupTest = (props?: ILoader) =>
    Enzyme.shallow(<Loader {...defaultProps} {...props} />);
  const LIGHT_LOADER_CLASSNAME = "loader--light";

  it("should not be light by default", () => {
    const wrapper = setupTest();
    expect(
      wrapper.find("[data-testid='loader']").prop("className")
    ).not.toContain(LIGHT_LOADER_CLASSNAME);
  });

  it("should be light if isLight passed as true", () => {
    const wrapper = setupTest({ isLight: true });
    expect(wrapper.find("[data-testid='loader']").prop("className")).toContain(
      LIGHT_LOADER_CLASSNAME
    );
  });
});
