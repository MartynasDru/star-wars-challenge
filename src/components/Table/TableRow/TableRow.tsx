import React from "react";
import { TableRow as MUITableRow, TableCell } from "@material-ui/core";

import styles from "./TableRow.module.scss";

export interface ITableRow {
  rowColumns: Array<string | number>;
  onRowClick: () => void;
}

const TableRow = ({ rowColumns, onRowClick }: ITableRow) => {
  return (
    <MUITableRow
      classes={{
        root: styles["table-row"],
      }}
      onClick={onRowClick}
      hover
    >
      {rowColumns.map((columnValue) => (
        <TableCell key={columnValue} component="th" scope="row">
          {columnValue}
        </TableCell>
      ))}
    </MUITableRow>
  );
};

export default TableRow;
