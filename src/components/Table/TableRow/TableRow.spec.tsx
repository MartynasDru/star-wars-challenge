import React from "react";
import Enzyme from "enzyme";
import {
  Table,
  TableBody,
  TableRow as MUITableRow,
  TableCell,
} from "@material-ui/core";

import TableRow, { ITableRow } from "./TableRow";

const defaultProps = {
  rowColumns: [],
  onRowClick: jest.fn(),
} as ITableRow;

describe("TableRow", () => {
  const setupTest = (props?: ITableRow) =>
    Enzyme.mount(
      <Table>
        <TableBody>
          <TableRow {...defaultProps} {...props} />
        </TableBody>
      </Table>
    );

  it("should render as many row columns as there is passed", () => {
    const rowColumns = ["Column-1", "Column-2", "Column-3"];
    const wrapper = setupTest({ ...defaultProps, rowColumns });

    const tableColumns = wrapper.find(TableCell);
    expect(tableColumns.length).toBe(rowColumns.length);
    tableColumns.forEach((column, index) => {
      expect(tableColumns.at(index).text()).toBe(rowColumns[index]);
    });
  });

  it("should call onRowClick if row clicked", () => {
    const wrapper = setupTest();
    wrapper.find(MUITableRow).at(0).simulate("click");
    expect(defaultProps.onRowClick).toHaveBeenCalled();
  });
});
