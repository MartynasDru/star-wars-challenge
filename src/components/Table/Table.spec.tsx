import React from "react";
import Enzyme from "enzyme";
import { TableBody, TableRow } from "@material-ui/core";

import Table, { ITable } from "./Table";

const defaultProps = {
  headerColumns: [],
  rowsData: [],
  onRowClick: jest.fn(),
} as ITable;

describe("Table", () => {
  const setupTest = (props?: ITable) =>
    Enzyme.mount(<Table {...defaultProps} {...props} />);

  it("should render as many header columns as there was passed", () => {
    const headerColumns = ["Column-1", "Column-2", "Column-3"];
    const wrapper = setupTest({ ...defaultProps, headerColumns });

    headerColumns.forEach((column, index) => {
      expect(wrapper.find(`[data-testid='${column}']`).length).toBe(
        headerColumns.length
      );
      expect(wrapper.find(`[data-testid='${column}']`).at(index).text()).toBe(
        column
      );
    });
  });

  it("should render as many table rows as there was passed", () => {
    const rowsData = [
      { rowId: "test-1", rowData: ["Row-1-Column-1"] },
      { rowId: "test-2", rowData: ["Row-2-Column-1"] },
      { rowId: "test-3", rowData: ["Row-3-Column-1"] },
    ];
    const wrapper = setupTest({ ...defaultProps, rowsData });

    const tableBody = wrapper.find(TableBody);
    let totalRows = 0;
    rowsData.forEach((rowData) => {
      totalRows += rowData.rowData.length;
    });
    expect(tableBody.find(TableRow).length).toBe(totalRows);
  });
});
