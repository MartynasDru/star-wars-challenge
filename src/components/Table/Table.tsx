import React from "react";
import {
  TableBody,
  TableCell,
  TableHead,
  TableRow as MUITableRow,
  Table as MUITable,
} from "@material-ui/core";

import TableRow from "./TableRow/TableRow";
import styles from "./Table.module.scss";

interface IRowData {
  rowId: string;
  rowData: Array<string>;
}

export type IAllRowsData = Array<IRowData>;

export interface ITable {
  headerColumns: Array<string>;
  rowsData: IAllRowsData;
  onRowClick: (rowId: string) => void;
}

const Table = ({ headerColumns, rowsData, onRowClick }: ITable) => (
  <MUITable
    classes={{
      root: styles["table"],
    }}
  >
    <TableHead>
      <MUITableRow>
        {Object.values(headerColumns).map((tableColumnNaming) => (
          <TableCell
            data-testid={tableColumnNaming}
            key={tableColumnNaming}
            classes={{
              root: styles["table__cell"],
            }}
          >
            {tableColumnNaming}
          </TableCell>
        ))}
      </MUITableRow>
    </TableHead>
    <TableBody>
      {rowsData.map((rowData) => (
        <TableRow
          key={rowData.rowId}
          rowColumns={rowData.rowData}
          onRowClick={() => onRowClick(rowData.rowId)}
        />
      ))}
    </TableBody>
  </MUITable>
);

export default Table;
