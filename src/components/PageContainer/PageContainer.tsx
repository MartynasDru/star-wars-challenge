import React from "react";

import styles from "./PageContainer.module.scss";

export interface IPageContainer {
  title: string;
  description: string;
  children: JSX.Element | Array<JSX.Element>;
}

const PageContainer = ({ title, description, children }: IPageContainer) => (
  <div className={styles["page-container"]}>
    <h1 data-testid="title" className={styles["page-container__title"]}>
      {title}
    </h1>
    <p
      data-testid="description"
      className={styles["page-container__description"]}
    >
      {description}
    </p>
    {children}
  </div>
);

export default PageContainer;
