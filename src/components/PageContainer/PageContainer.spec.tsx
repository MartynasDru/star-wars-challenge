import React from "react";
import Enzyme from "enzyme";

import PageContainer, { IPageContainer } from "./PageContainer";

const defaultProps = {
  title: "",
  description: "",
  children: <div data-testid="child">Child</div>,
} as IPageContainer;

describe("PageContainer", () => {
  const setupTest = (props?: IPageContainer) =>
    Enzyme.shallow(<PageContainer {...defaultProps} {...props} />);

  it("should render passed title", () => {
    const title = "Test title";
    const wrapper = setupTest({ ...defaultProps, title });
    expect(wrapper.find("[data-testid='title']").text()).toBe(title);
  });

  it("should render passed description", () => {
    const description = "Test description";
    const wrapper = setupTest({ ...defaultProps, description });
    expect(wrapper.find("[data-testid='description']").text()).toBe(
      description
    );
  });

  it("should render passed children", () => {
    const wrapper = setupTest();
    expect(wrapper.find("[data-testid='child']").length).toBe(1);
  });
});
