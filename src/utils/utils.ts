export const classNames = (
  args: Array<string | number | undefined | boolean | null> | null
) => args?.filter(Boolean)?.join(" ") || "";
